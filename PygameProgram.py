#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from Config import Config

from controllers import GamepadManager

class PygameProgram( object ):
    def __init__( self ):
        self.fpsClock = None
        self.windowSurface = None
        self.currentState = None
        self.screenWidth = 1280
        self.screenHeight = 720
        self.isDone = False
        self.fps = 30
        self.clearColor = pygame.Color( 100, 100, 100 )

        self.mouseX = 0
        self.mouseY = 0

    def Setup( self, gamename ):
        pygame.init()
        self.fpsClock = pygame.time.Clock()
        self.windowSurface = pygame.display.set_mode( ( self.screenWidth, self.screenHeight ) )
        pygame.display.set_caption( gamename )

        GamepadManager.Setup()

    def Teardown( self ):
        pygame.quit()
        sys.exit()

    def GetScreenDimensions( self ):
        return self.screenWidth, self.screenHeight

    def IsDone( self ):
        return self.isDone

    def GetEvents( self ):
        events = pygame.event.get()

        for event in events:
            if event.type == QUIT:
                self.isDone = True

        return events

    def GetKeysPressed( self ):
        return pygame.key.get_pressed()

    def Draw( self, image, position ):
        self.windowSurface.blit( image, position )

    def GetWindow( self ):
        return self.windowSurface

    def CycleBegin( self ):
        self.windowSurface.fill( self.clearColor )

    def CycleEnd( self ):
        pygame.display.update()
        self.fpsClock.tick( self.fps )
