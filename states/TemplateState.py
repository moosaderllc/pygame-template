#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from BaseState import BaseState

class TemplateState( BaseState ):
    def __init__( self ):
        self.stateName = "templateState"
        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""        
        
    def Update( self ):        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if event.type == MOUSEBUTTONUP:
                mouseX, mouseY = event.pos
                
                if ( clickAction != "" ):
                    self.gotoState = clickAction
                
    
    def Draw( self, windowSurface ):
        pass
    
    def Clear( self ):
        pass
