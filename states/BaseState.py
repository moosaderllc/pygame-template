import pygame, sys
from pygame.locals import *

class BaseState:
    def __init__( self ):
        self.stateName = "basestate"
        self.gotoState = ""
        
    def Update( self ):
        print( "Update" )
    
    def Draw( self, windowSurface ):
        print( "Draw" )

    def GotoState( self ):
        return self.gotoState

    def Clear( self ):
        print( "Clear" )
