#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import random

from BaseState import BaseState
from Config import Config

from controllers import GamepadManager

class SimpleGameState( BaseState ):
    def __init__( self ):
        self.stateName = "simplegamestate"

    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""

        self.background = pygame.image.load( "content/graphics/terrain/house.png" )
        
        self.backgroundColor = pygame.Color( 50, 50, 50 )

    def Update( self ):
        for event in pygame.event.get():
            if ( event.type == QUIT ):
                pygame.quit()
                sys.exit()

        GamepadManager.Update()
        for gamepad in GamepadManager.GetAllGamepads():
            joystickIndex = gamepad.GetJoystickIndex()
            
            if ( gamepad.IsButtonPressed( gamepad.Button["B"] ) ):
                gamepad.SetCooldown( 10 )
                


    def Draw( self, windowSurface ):
        windowSurface.fill( self.backgroundColor )
        windowSurface.blit( self.background, ( 0, 0 ) )

        GamepadManager.Draw( windowSurface )


    def Clear( self ):
        pass
