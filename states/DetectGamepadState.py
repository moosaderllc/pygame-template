#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import random

from BaseState import BaseState
from Config import Config
from ui import Button

from controllers import GamepadManager
from controllers import Mouse

from utilities import Logger

class DetectGamepadState( BaseState ):
    def __init__( self ):
        self.stateName = "detectgamepadstate"
        self.mouse = Mouse()

    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""
        
        # Which game setup step are we in?
        self.step = 1
        
        # How many players will there be?
        self.totalPlayers = 0
        self.setupPlayer = 0

        self.background = pygame.image.load( "content/graphics/ui/player-select-background.png" )
        self.buttonBackground = pygame.image.load( "content/graphics/ui/buttonbg_small.png" )
        self.mouseIcons = {
            "unknown" : pygame.image.load( "content/graphics/ui/cursor-neutral.png" ),
            "basel" :   pygame.image.load( "content/graphics/ui/cursor-basel.png" ),
            "natalie" : pygame.image.load( "content/graphics/ui/cursor-natalie.png" ),
            "rose" :    pygame.image.load( "content/graphics/ui/cursor-rose.png" ),
            "ruby" :    pygame.image.load( "content/graphics/ui/cursor-ruby.png" ),
            }
            
        self.mouse.SetImage( self.mouseIcons[ "unknown" ] )

        # How many joysticks are activated?
        self.joystickStates = {}
        for j in range( pygame.joystick.get_count() ):
            self.joystickStates[ j ] = {
                "state" : "disconnected",
                "player" : 0
            }
        
        # Fonts and colors
        self.backgroundColor = pygame.Color( 50, 50, 50 )
        self.textColor = pygame.Color( 105, 75, 0 )
        self.fntMain = pygame.font.Font( "content/fonts/Carlito-Bold.ttf", 32 )
        self.fntHeader = pygame.font.Font( "content/fonts/ThatNogoFontCasual.ttf", 100 )
        
        # UI buttons and labels
        self.buttons = []
        self.miscLabels = []
        
        self.SetupMenu()
        
        # Info on the players (player names + color codes)
        self.playerInfo = Config.GetOption( "player-info" )


    def SetupMenu( self ):
        self.buttons = []
        self.miscLabels = []
        
        # Step 1:   Get the amount of players who are going to play
        #           Only "player 1" can control it here.
        if ( self.step == 1 ):
            self.miscLabels.append( { 
                "label" : self.fntHeader.render( "How many people?", True, self.textColor ), 
                "position" : ( 20, 20 ) } )
            
            x = 85
            inc = 300
            self.buttons.append( Button( {
                "button_texture" : self.buttonBackground,
                "button_position" : ( x + (0 * inc), 200 ),
                "button_action" : "1 player",
                "button_type" : "confirm",
                "label_text" : "1",
                "label_position" : ( 75, 50 ),
                "label_font" : self.fntHeader,
                "label_color" : pygame.Color( 210, 220, 100 )
                } ) )
                
            self.buttons.append( Button( {
                "button_texture" : self.buttonBackground,
                "button_position" : ( x + (1 * inc), 200 ),
                "button_action" : "2 players",
                "button_type" : "confirm",
                "label_text" : "2",
                "label_position" : ( 75, 50 ),
                "label_font" : self.fntHeader,
                "label_color" : pygame.Color( 210, 220, 100 )
                } ) )
                
            self.buttons.append( Button( {
                "button_texture" : self.buttonBackground,
                "button_position" : ( x + (2 * inc), 200 ),
                "button_action" : "3 player",
                "button_type" : "confirm",
                "label_text" : "3",
                "label_position" : ( 75, 50 ),
                "label_font" : self.fntHeader,
                "label_color" : pygame.Color( 210, 220, 100 )
                } ) )
                
            self.buttons.append( Button( {
                "button_texture" : self.buttonBackground,
                "button_position" : ( x + (3 * inc), 200 ),
                "button_action" : "4 player",
                "button_type" : "confirm",
                "label_text" : "4",
                "label_position" : ( 75, 50 ),
                "label_font" : self.fntHeader,
                "label_color" : pygame.Color( 210, 220, 100 )
                } ) )
        
        # Step 2:   Let each player choose their profile ONE AT A TIME.
        #           If all the kids are controlling cursors at once, nobody
        #           will know who is who and there will be mass chaos.
        elif ( self.step == 2 ):
            self.buttons = []
            self.miscLabels = []
            
            self.miscLabels.append( { 
                "label" : self.fntHeader.render( "Player " + str( self.setupPlayer ) + " choose your profile", True, self.textColor ), 
                "position" : ( 20, 20 ) } )
                
            self.miscLabels.append( { "label" : self.fntMain.render( "Press [LEFT] and [RIGHT] to select your name.", True, self.textColor ), "position" : ( 320, 130 ) } )
            self.miscLabels.append( { "label" : self.fntMain.render( "Press [START] to confirm.", True, self.textColor ), "position" : ( 320, 160 ) } )

            self.playerLabels = []
            self.statusLabels = []
            self.playerNames = []
            self.playerSprites = []
            self.okChecks = []
            
            x = 85
            inc = 300
            self.buttons.append( Button( {
                "button_texture" : self.buttonBackground,
                "button_position" : ( x + (0 * inc), 200 ),
                "button_action" : self.playerInfo[ 0 ][ "name" ],
                "button_type" : "confirm",
                "label_text" : self.playerInfo[ 0 ][ "name" ],
                "label_position" : ( 10, 50 ),
                "label_font" : self.fntMain,
                "label_color" : self.playerInfo[ 0 ][ "color" ]
                } ) )
                
            self.buttons.append( Button( {
                "button_texture" : self.buttonBackground,
                "button_position" : ( x + (1 * inc), 200 ),
                "button_action" : self.playerInfo[ 1 ][ "name" ],
                "button_type" : "confirm",
                "label_text" : self.playerInfo[ 1 ][ "name" ],
                "label_position" : ( 10, 50 ),
                "label_font" : self.fntMain,
                "label_color" : self.playerInfo[ 1 ][ "color" ]
                } ) )
                
            self.buttons.append( Button( {
                "button_texture" : self.buttonBackground,
                "button_position" : ( x + (2 * inc), 200 ),
                "button_action" : self.playerInfo[ 2 ][ "name" ],
                "button_type" : "confirm",
                "label_text" : self.playerInfo[ 2 ][ "name" ],
                "label_position" : ( 10, 50 ),
                "label_font" : self.fntMain,
                "label_color" : self.playerInfo[ 2 ][ "color" ]
                } ) )
                
            self.buttons.append( Button( {
                "button_texture" : self.buttonBackground,
                "button_position" : ( x + (3 * inc), 200 ),
                "button_action" : self.playerInfo[ 3 ][ "name" ],
                "button_type" : "confirm",
                "label_text" : self.playerInfo[ 3 ][ "name" ],
                "label_position" : ( 10, 50 ),
                "label_font" : self.fntMain,
                "label_color" : self.playerInfo[ 3 ][ "color" ]
                } ) )
            
            
            # for p in range( 4 ):
                # self.playerLabels.append( self.fntMain.render( "Player " + str( p+1 ), False, self.textColor ) )
                # self.statusLabels.append( self.fntMain.render( "Disconnected", False, self.textColor ) )
                # self.playerNames.append( self.fntMain.render( "", False, self.textColor ) )
                # self.playerSprites.append( pygame.image.load( "content/graphics/ui/avatar-unknown.png" ) )
                # self.okChecks.append( None )
                # Config.SetOption( "player" + str(p+1), "disconnected" )

    def Update( self ):
        events = pygame.event.get()
        for event in events:
            if ( event.type == QUIT ):
                pygame.quit()
                sys.exit()

        GamepadManager.Update()
        self.mouse.Update( events )
        
        # Choosing how many players
        if ( self.step == 1 ):
            currentGamepad = GamepadManager.GetGamepad( 0 )
            
            clicked, mouseX, mouseY = self.mouse.IsLeftClick()
            
            if ( currentGamepad is not None ):
                # Let the gamepad control the mouse too
                if ( currentGamepad.IsDirectionLeft( True ) ):    self.mouse.MoveLeft( 5 )
                if ( currentGamepad.IsDirectionRight( True ) ):   self.mouse.MoveRight( 5 )
                if ( currentGamepad.IsDirectionUp( True ) ):      self.mouse.MoveUp( 5 )
                if ( currentGamepad.IsDirectionDown( True ) ):    self.mouse.MoveDown( 5 )
            
                if ( currentGamepad.IsButtonPressed( currentGamepad.Button[ "B" ] ) ):
                    clicked = True
            
            
            updated = True
            if ( clicked ):
                # Only let one player select how many total players
                for button in self.buttons:
                    if ( button.IsClicked( mouseX, mouseY ) ):
                        buttonAction = button.GetAction()
                        print( buttonAction )
                        
                        if      ( buttonAction == "1 player" ):    self.totalPlayers = 1                            
                        elif    ( buttonAction == "2 player" ):    self.totalPlayers = 2                            
                        elif    ( buttonAction == "3 player" ):    self.totalPlayers = 3                            
                        elif    ( buttonAction == "4 player" ):    self.totalPlayers = 4
                            
                        else:
                            updated = False
                            
                        for i in range( self.totalPlayers ):
                            self.joystickStates[ i ]["state"] = "connected"
            else:
                updated = False
            
            # Did we change from Step 1 to Step 2?
            if ( updated ):
                self.step = 2
                self.setupPlayer = 1
                self.SetupMenu()
                            
                        
        # Choosing profiles
        elif ( self.step == 2 ):
            
            # We're storing the setupPlayer as 1, 2, 3, 4, so it's offset by 1.
            currentGamepad = GamepadManager.GetGamepad( self.setupPlayer - 1 )
            
            # Let the gamepad control the mouse too
            if ( currentGamepad.IsDirectionLeft( True ) ):    self.mouse.MoveLeft( 5 )
            if ( currentGamepad.IsDirectionRight( True ) ):   self.mouse.MoveRight( 5 )
            if ( currentGamepad.IsDirectionUp( True ) ):      self.mouse.MoveUp( 5 )
            if ( currentGamepad.IsDirectionDown( True ) ):    self.mouse.MoveDown( 5 )
            
            clicked, mouseX, mouseY = self.mouse.IsLeftClick()
            
            if ( currentGamepad.IsButtonPressed( currentGamepad.Button[ "B" ] ) ):
                clicked = True
                
            updated = True
            if ( clicked ):
                for button in self.buttons:
                    if ( button.IsClicked( mouseX, mouseY ) ):
                        buttonAction = button.GetAction()
                        
                        # Set who is playing on this controller
                        
                        if      ( buttonAction == self.playerInfo[ 0 ][ "name" ] ):    self.joystickStates[ self.setupPlayer ][ "player" ] = 0
                        elif    ( buttonAction == self.playerInfo[ 1 ][ "name" ] ):    self.joystickStates[ self.setupPlayer ][ "player" ] = 1
                        elif    ( buttonAction == self.playerInfo[ 2 ][ "name" ] ):    self.joystickStates[ self.setupPlayer ][ "player" ] = 2
                        elif    ( buttonAction == self.playerInfo[ 3 ][ "name" ] ):    self.joystickStates[ self.setupPlayer ][ "player" ] = 3
                        else:
                            updated = False
            else:
                updated = False
                            
                    
            if ( updated ):
                self.setupPlayer += 1
                if ( self.setupPlayer > self.totalPlayers ):
                    # We're done with this step!
                    self.step = 3
                        
                            


    def Draw( self, windowSurface ):
        windowSurface.fill( self.backgroundColor )
        windowSurface.blit( self.background, ( 0, 0 ) )

        # Display the Gamepad helper - an image of everyone's gamepads
        GamepadManager.Draw( windowSurface )

        for label in self.miscLabels:
            windowSurface.blit( label["label"], label["position"] )

        if ( self.step == 1 ):
            for btn in self.buttons:
                btn.Draw( windowSurface )
            
        elif ( self.step == 2 ):
            labelx = 50
            labely = 300
            labelinc = 1280/4

            # for p in range( 4 ):
                # windowSurface.blit( self.playerSprites[ p ], ( labelx, labely ) )
                # windowSurface.blit( self.playerLabels[ p ], ( labelx + 50, labely + 250 ) )
                # windowSurface.blit( self.playerNames[ p ], ( labelx + 50, labely + 280 ) )
                
                # if ( self.okChecks[ p ] is not None ):
                    # windowSurface.blit( self.okChecks[ p ], ( labelx + 100, labely - 5 ) )

                # labelx += labelinc

        
        # Draw the mouse cursor
        self.mouse.Draw( windowSurface )

    def Clear( self ):
        pass
