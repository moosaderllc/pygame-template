#!/usr/bin/python
# -*- coding: utf-8 -*-

from PygameProgram import PygameProgram
from PlayableCharacter import PlayableCharacter

from states import DetectGamepadState
from states import SimpleGameState

from Config import Config

import pygame, sys
from pygame.locals import *

game = PygameProgram()
game.Setup( "Pygame Template" )

Config.SetOption( "player-info", [
    {
        "name" : "Basel",
        "color" : pygame.Color( 158, 255, 86 )
    },
    {
        "name" : "Natalie",
        "color" : pygame.Color( 105, 186, 255 )
    },
    {
        "name" : "Rose",
        "color" : pygame.Color( 255, 86, 189 )
    },
    {
        "name" : "Ruby",
        "color" : pygame.Color( 0, 216, 188 )
    }
] )

screenWidth, screenHeight = game.GetScreenDimensions()

cat = PlayableCharacter()
cat.Setup( pygame.image.load( "content/graphics/character.png" ), 600, 600, 45, 49 )

state = DetectGamepadState()
state.Setup( screenWidth, screenHeight )

while ( game.IsDone() is False ):
    game.CycleBegin()
    
    # Events
    events = game.GetEvents()
    state.Update()
            
    state.Draw( game.GetWindow() )
    
    #cat.HandleInput( game.GetKeysPressed() )        
    #cat.Draw( game.GetWindow() )
    
    game.CycleEnd()


game.Teardown()
