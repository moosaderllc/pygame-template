#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from Gamepad import Gamepad

class GamepadManager():
    totalGamepads = 0
    gamepads = []
    
    @classmethod
    def Setup( pyClass ):
        pygame.joystick.init()
        
        totalGamepads = pygame.joystick.get_count()
        
        gamepadImage = pygame.image.load( "content/graphics/ui/gamepad-reference-small.png" )
        
        for i in range( totalGamepads ):
            newGamepad = Gamepad()
            newGamepad.Setup( pygame.joystick.Joystick( i ), i )
            x = (i * 1280/4) + 100
            y = 630
            newGamepad.SetupHudReference( gamepadImage, pygame.Rect( x, y, 110, 76 ) )
            pyClass.gamepads.append( newGamepad )
            
        print( str( totalGamepads ) + " total gamepads" )
    
    @classmethod
    def Update( pyClass ):
        for gamepad in pyClass.gamepads:
            gamepad.Update()
            
    @classmethod
    def Draw( pyClass, windowSurface ):
        for gamepad in pyClass.gamepads:
            gamepad.Draw( windowSurface )
    
    @classmethod
    def GetGamepad( pyClass, index ):
        if ( index >= len( pyClass.gamepads ) ):
            return None
        return pyClass.gamepads[ index ]
        
    @classmethod
    def GetAllGamepads( pyClass ):
        return pyClass.gamepads
        
    @classmethod
    def GetTotalGamepads( pyClass ):
        return pyClass.totalGamepads
