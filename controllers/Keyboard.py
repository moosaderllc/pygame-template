#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

class Keyboard( object ):
    def __init__( self ):
        self.joystickIndex
        self.joystick = None
        self.cooldown = 0

    def Setup( self, joystickRef, index ):
        self.joystickIndex = index
        self.joystick = joystickRef
        self.joystick.init()
        
    def IsPressed( self, buttonIndex ):
        return self.joystick.get_button( buttonIndex ) == True
        
    def IsDirectionLeft( self ):
        return self.joystick.get_axis( 0 ) < 0
    
    def IsDirectionRight( self ):
        return self.joystick.get_axis( 0 ) > 0
    
    def IsDirectionUp( self ):
        return self.joystick.get_axis( 1 ) < 0
        
    def IsDirectionDown( self ):
        return self.joystick.get_axis( 1 ) > 0

    # For button cooldowns (like in menus) where you don't want
    # something to get rapid-fire triggered.
    def GetCooldown( self ):
        return self.cooldown
    
    def SetCooldown( self, value ):
        self.cooldown = value
        
    def Update( self ):
        if ( self.cooldown > 0 ):
            self.cooldown -= 1
