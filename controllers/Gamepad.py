#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *


class Gamepad( object ):
    def __init__( self ):
        self.joystickIndex = None
        self.joystick = None
        self.cooldown = 0
        self.drawReference = True
        self.referenceImage = None
        self.referencePosition = pygame.Rect( 0, 0, 220, 152 )
        
        self.Button = { 
            "X" : 0,
            "A" : 1,
            "B" : 2,
            "Y" : 3,
            "L" : 4,
            "R" : 5,
            "SELECT" : 8,
            "START" : 9
            }

    def Setup( self, joystickRef, index ):
        self.joystickIndex = index
        self.joystick = joystickRef
        self.joystick.init()
        
    def SetupHudReference( self, image, position ):
        self.referenceImage = image
        self.referencePosition = position
        
    def Draw( self, windowSurface ):
        if ( self.drawReference and self.referenceImage is not None ):
            # Base controller
            w = self.referencePosition.w
            h = self.referencePosition.h
            
            windowSurface.blit( self.referenceImage, self.referencePosition, pygame.Rect( 0, 0, w, h ) )
            
            # Highlight buttons if reference is on
            for b in range( self.joystick.get_numbuttons() ):
                if ( self.IsButtonPressed( b, True ) ):
                    windowSurface.blit( self.referenceImage, self.referencePosition, pygame.Rect( (b+1) * w, 0, w, h ) )
            
            if ( self.IsDirectionLeft( True ) ):
                windowSurface.blit( self.referenceImage, self.referencePosition, pygame.Rect( 11 * w, 0, w, h ) )
            
            elif ( self.IsDirectionRight( True ) ):
                windowSurface.blit( self.referenceImage, self.referencePosition, pygame.Rect( 12 * w, 0, w, h ) )
                
            if ( self.IsDirectionUp( True ) ):
                windowSurface.blit( self.referenceImage, self.referencePosition, pygame.Rect( 13 * w, 0, w, h ) )
            
            elif ( self.IsDirectionDown( True ) ):
                windowSurface.blit( self.referenceImage, self.referencePosition, pygame.Rect( 14 * w, 0, w, h ) )
            
        
    def GetJoystickIndex( self ):
        return self.joystickIndex
    
    def GetTotalButtons( self ):
        return self.joystick.get_numbuttons()
        
    def IsButtonPressed( self, buttonIndex, ignoreCooldown = False ):
        return ( self.cooldown == 0 or ignoreCooldown ) and self.joystick.get_button( buttonIndex ) == True
    
    def IsAnyButtonPressed( self, ignoreCooldown = False ):
        if self.cooldown > 0 and ignoreCooldown == False:
            return False
            
        for b in range( self.joystick.get_numbuttons() ):
            if ( self.joystick.get_button( b ) == True ):
                return True
                
        return False
        
    def IsDirectionLeft( self, ignoreCooldown = False ):
        return ( self.cooldown == 0 or ignoreCooldown ) and self.joystick.get_axis( 0 ) < 0
    
    def IsDirectionRight( self, ignoreCooldown = False ):
        return ( self.cooldown == 0 or ignoreCooldown ) and self.joystick.get_axis( 0 ) > 0
    
    def IsDirectionUp( self, ignoreCooldown = False ):
        return ( self.cooldown == 0 or ignoreCooldown ) and self.joystick.get_axis( 1 ) < 0
        
    def IsDirectionDown( self, ignoreCooldown = False ):
        return ( self.cooldown == 0 or ignoreCooldown ) and self.joystick.get_axis( 1 ) > 0

    # For button cooldowns (like in menus) where you don't want
    # something to get rapid-fire triggered.
    def GetCooldown( self ):
        return self.cooldown
    
    def SetCooldown( self, value ):
        self.cooldown = value
        
    def Update( self ):
        if ( self.cooldown > 0 ): 
            self.cooldown -= 1
            
