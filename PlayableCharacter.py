#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

class PlayableCharacter( object ):
    def __init__( self ):
        self.mappings = {
            "up" : K_UP,
            "down" : K_DOWN,
            "left" : K_LEFT,
            "right" : K_RIGHT,
            "run" : K_LSHIFT
            }
        
        # For animations
        self.actionMappings = {}
        
        self.walkSpeed = 5
        self.runSpeed = 10
        self.speed = self.walkSpeed
        self.direction = "DOWN"
        self.isRunning = False
        self.allowDiagonal = True
                
        self.posRect = pygame.Rect( 0, 0, 16, 16 )
        self.frameRect = pygame.Rect( 0, 0, 16, 16 )
        self.image = None

        self.hp = 20
        self.maxHp = 20
        self.showHpBar = False      # For showing an HP bar indicator above character head
        self.label = None           # For if you want to display a name over this character
        
        self.animated = False       # Whether to update frames
        self.frame = 0
        self.maxFrame = 1
        self.animateSpeedWalk = 0.1
        self.animateSpeedRun = 0.1
    
    # Setup functions
    def Setup( self, image, x, y, w, h ):
        self.image = image
        self.posRect = pygame.Rect( x, y, w, h )
        self.frameRect = pygame.Rect( 0, 0, w, h )
        self.colRect = pygame.Rect( 0, 0, 16, 16 )
        
    def SetInputs( self, mappings, allowDiagonal=False ):
        self.mappings = mappings
        self.allowDiagonal = allowDiagonal

    def SetDimensions( self, width, height ):
        self.posRect.width = width
        self.posRect.height = height

        self.frameRect.width = width
        self.frameRect.height = height
        
    def SetupAnimation( self, maxFrame, animateSpeedWalk, animateSpeedRun ):
        self.animated = True
        self.maxFrame = maxFrame
        self.animateSpeedWalk = animateSpeedWalk
        self.animateSpeedRun = animateSpeedRun
        
    def SetAnimationMappings( self, options ):
        # key : row#
        self.actionMappings = options
    
    def ShowHPBar( self, value ):
        self.showHpBar = value

    def SetName( self, name ):
        self.name = name
        self.label = Label()
        self.label.Setup( {
            "font" : FontManager.Get( "label" ),
            "text" : self.name,
            "position" : ( self.posRect.x, self.posRect.y - 40 ),
            "color" : pygame.Color( 255, 255, 255 ),
            "style" : "outline",
            "secondary_color" : pygame.Color( 0, 0, 0 )
            } )

    # During gameloop functions
    def HandleInput( self, keys ):
        if ( keys[ self.mappings[ "run" ] ] ):
            self.isRunning = True
        else:
            self.isRunning = False

        vMove = ""
        hMove = ""
            
        if ( keys[ self.mappings[ "up" ] ] ):
            vMove = "UP"
            
        if ( keys[ self.mappings[ "down" ] ] ):
            vMove = "DOWN"
            
        if ( keys[ self.mappings[ "left" ] ] ):
            hMove = "LEFT"
            
        if ( keys[ self.mappings[ "right" ] ] ):
            hMove = "RIGHT"

            
        if ( self.allowDiagonal ):
            return self.Move( vMove, hMove )
        else:
            return self.Move( hMove, "" )

    def ChangeFrame( self, x, y ):
        self.frameRect.x = x
        self.frameRect.y = y

    def Move( self, direction1, direction2="" ):        
        if ( self.isRunning ):
            self.speed = self.runSpeed
        else:
            self.speed = self.walkSpeed

        if ( direction1 != "" and direction2 != "" ):
            # Use sqrt(2)/2 to adjust speed for 45 degree angle
            self.speed *= 0.71

        moveX = 0
        moveY = 0
        
        if ( direction1 == "UP" or direction2 == "UP" ):
            moveY = -self.speed
            
        if ( direction1 == "DOWN" or direction2 == "DOWN" ):
            moveY = self.speed
            
        if ( direction1 == "LEFT" or direction2 == "LEFT" ):
            moveX = -self.speed
            
        if ( direction1 == "RIGHT" or direction2 == "RIGHT" ):
            moveX = self.speed

        movedX, movedY = self.MoveXY( moveX, moveY )
        self.direction = direction1
        if ( self.direction == "" ):
            self.direction = direction2

        return movedX, movedY

    def ChangeAnimationAction( self, key ):
        if ( key is not "" ):
            self.currentAction = key

    def GetAnimationAction( self ):
        return self.currentAction

    def Animate( self ):
        if ( self.animated == False ):
            return

        if ( self.isRunning ):
            self.frame = self.frame + self.animateSpeedRun
        else:
            self.frame = self.frame + self.animateSpeedWalk
        
        if ( self.frame >= self.maxFrame ):
            self.frame = 0

        self.frameRect.y = self.actionMappings[ self.currentAction ] * self.frameRect.height
            
        frameX = int( self.frame ) * self.frameRect.width
        frameY = int( self.frameRect.y )

        self.ChangeFrame( frameX, frameY )

    def MoveXY( self, xAmount, yAmount ):
        # If there is no collision, we can do the movement.
        self.posRect.x = self.posRect.x + xAmount
        self.posRect.y = self.posRect.y + yAmount

        if ( self.label is not None ):
            self.label.SetPosition( self.posRect.x, self.posRect.y - 40 )

        return xAmount, yAmount


    def Update( self ):
        if ( self.animated ):
            self.Animate()
            
            
    def Draw( self, window ):
        posRect = self.posRect
            
        if ( self.image is not None ):
            window.blit( self.image, posRect, self.frameRect )

            if ( self.label is not None ):
                self.label.Draw( window )

            if ( self.showHpBar == True ):
                # HP Bar
                barWidth = 64
                ratio = float( self.hp ) / float( self.maxHp )

                pygame.draw.rect( window, pygame.Color( 0, 0, 0 ), ( posRect.x-1, posRect.y - 11, barWidth+2, 5+2 ) )
                pygame.draw.rect( window, pygame.Color( 0, 255, 0 ), ( posRect.x, posRect.y - 10, ratio * barWidth, 5 ) )
