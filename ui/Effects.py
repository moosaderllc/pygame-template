#!/usr/bin/python
# -*- coding: utf-8 -*-

from Label import Label
from utilities import Logger

class Effects:
    def __init__( self ):
        self.effects = []
        self.timerSpeed = 1
        self.moveAmount = 2

    def AddLabelEffect( self, options ):
        newEffect = {}
        newEffect["effect"] = options["effect"]
        newEffect["label"] = Label()
        newEffect["label"].Setup( options["label"] )
        
        if ( "timer" not in options ):
            newEffect["timer"] = 20
        else:
            newEffect["timer"] = options["timer"]
        
        self.effects.append( newEffect )
        Logger.Write( "Create a new effect with text: " + options["label"]["label_text"], "Effects::AddLabelEffect", "#fcb3b3" )

    def Update( self ):
        deleteme = []
        for key in range(0, len(self.effects)):
            if ( self.effects[key]["timer"] > 0 ):
                self.effects[key]["timer"] -= self.timerSpeed
                
                if ( self.effects[key]["effect"] == "floatup" ):
                    rect = self.effects[key]["label"].GetPosition()
                    self.effects[key]["label"].SetPosition( rect[0], rect[1] - self.moveAmount )
                
            else:
                # Remove me
                deleteme.append( key )
            
        for item in deleteme:
            self.effects.pop( item )

    def Draw( self, window ):
        for effect in self.effects:
            effect["label"].Draw( window )
