#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys, math
from pygame.locals import *

from Label import Label
from Image import Image
from Button import Button
from PercentBar import PercentBar
from utilities import Logger

class Menu:
    def __init__( self ):
        Logger.Write( "Menu created", "Menu::__init__" )
        self.bgColor = None
        self.hrColor = pygame.Color( 100, 100, 100 )
        self.rect = None
        self.name = ""
        self.isOpen = False
        
        self.elements = {}
        self.buttons = {}
        self.separators = []
        
    def SetName( self, name ):
        self.name = name
        
    def GetName( self ):
        return self.name
        
    def IsOpen( self ):
        return self.isOpen
        
    def Show( self ):
        self.isOpen = True
        
    def HasElement( self, key ):
        return ( key in self.elements )

    def SetBackground( self, options ):
        self.bgColor = options["color"]
        self.rect = ( options["x"], options["y"], options["width"], options["height"] )

    def AddPercentBar( self, key, options ):
        if ( key in self.elements ):
            return
        self.elements[key] = PercentBar()
        self.elements[key].Setup( options )
        
    def UpdatePercentBarAmount( self, key, value ):
        self.elements[key].ChangePercentage( value )

    def AddLabel( self, key, options ):
        if ( key in self.elements ):
            return
        self.elements[key] = Label()
        self.elements[key].Setup( options )
    
    def UpdateLabelText( self, key, text ):
        self.elements[key].ChangeText( text, text )

    def AddImage( self, key, options ):
        if ( key in self.elements ):
            return
        self.elements[key] = Image()
        self.elements[key].Setup( options )
        
    def UpdateImageTexture( self, key, texture ):
        self.elements[key].ChangeImage( texture )

    def AddButton( self, key, options ):
        if ( key in self.elements ):
            return
        self.buttons[key] = Button()
        self.buttons[key].Setup( options )
        
        self.Debug_PrintButtonKeys()

    def AddSeparator( self, options ):
        sep = { "y" : options["y"] }
        self.separators.append( sep )

    def RemoveElement( self, key ):
        self.elements.pop( key, None )
        self.buttons.pop( key, None )

    def Clear( self ):
        self.elements.clear()
        self.buttons.clear()
        self.bgColor = None
        self.rect = None
        self.name = ""
        self.isOpen = False
        
        self.Debug_PrintButtonKeys()
        
    def Debug_PrintButtonKeys( self ):
        strButtons = ""
        for key, button in self.buttons.iteritems():
            strButtons += key + "\t"
            
        Logger.Write( "Buttons in menu: " + strButtons, "Menu::Debug_PrintButtonKeys " + self.name )

    def ClickedButton( self, x, y ):
        clickKey = ""
        
        for key, button in self.buttons.iteritems():
            
            if ( button.IsClicked( x, y ) ):
                Logger.Write( "Button clicked: " + key, "Menu::ClickedButton " + self.name, True )
                clickKey = key
                break

        return clickKey

    def Draw( self, windowSurface ):
        # Draw background
        if ( self.bgColor is not None ):
            pygame.draw.rect( windowSurface, self.bgColor, self.rect )

        # Draw separators
        for sep in self.separators:
            pygame.draw.line( windowSurface, self.hrColor, ( self.rect[0], sep["y"] ), ( self.rect[0] + self.rect[2], sep["y"] ) )

        # Draw elements
        for key, element in self.elements.iteritems():
            element.Draw( windowSurface )
            
        # Draw buttons
        for key, button in self.buttons.iteritems():
            button.Draw( windowSurface )


