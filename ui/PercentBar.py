#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys, math
from pygame.locals import *

from copy import copy

class PercentBar:
    def __init__( self, options=None ):
        self.options = {}

        self.position = None
        self.barColor = None
        self.borderColor = pygame.Color( 0, 0, 0 )
        self.borderColor2 = pygame.Color( 255, 255, 255 )
        self.bgColor = pygame.Color( 50, 50, 50 )
        self.fullWidth = 0
        self.height = 0
        self.percent = 0

        if ( options is not None ):
            self.Setup( options )

    def Setup( self, options ):
        self.barColor = options["bar_color"]
        self.fullWidth = options["width"]
        self.height = options["height"]
        self.percent = options["initial_value"]
        self.position = options["position"]

    def ChangePercentage( self, value ):
        self.percent = value

    def Draw( self, windowSurface ):
        percent = self.percent / 100.0
        
        # Background
        pygame.draw.rect( windowSurface, self.bgColor, ( self.position[0], self.position[1], self.fullWidth, self.height ) )

        # Bar
        pygame.draw.rect( windowSurface, self.barColor, ( self.position[0], self.position[1], int( percent * self.fullWidth ), self.height ) )

        # Frame
        pygame.draw.line( windowSurface, self.borderColor, ( self.position[0], self.position[1] ), ( self.position[0] + self.fullWidth, self.position[1] ) )
        pygame.draw.line( windowSurface, self.borderColor, ( self.position[0], self.position[1] + self.height ), ( self.position[0] + self.fullWidth, self.position[1] + self.height ) )
        pygame.draw.line( windowSurface, self.borderColor, ( self.position[0], self.position[1] ), ( self.position[0], self.position[1] + self.height ) )
        pygame.draw.line( windowSurface, self.borderColor, ( self.position[0] + self.fullWidth, self.position[1] ), ( self.position[0] + self.fullWidth, self.position[1] + self.height ) )

        # Frame 2
        pygame.draw.line( windowSurface, self.borderColor2, ( self.position[0] - 1, self.position[1] - 1 ), ( self.position[0] + self.fullWidth + 1, self.position[1] - 1 ) )
        pygame.draw.line( windowSurface, self.borderColor2, ( self.position[0] - 1, self.position[1] + self.height + 1 ), ( self.position[0] + self.fullWidth, self.position[1] + self.height + 1 ) )
        pygame.draw.line( windowSurface, self.borderColor2, ( self.position[0] - 1, self.position[1] - 1 ), ( self.position[0] - 1, self.position[1] + self.height + 1 ) )
        pygame.draw.line( windowSurface, self.borderColor2, ( self.position[0] + self.fullWidth + 1, self.position[1] - 1 ), ( self.position[0] + self.fullWidth + 1, self.position[1] + self.height + 1 ) )

