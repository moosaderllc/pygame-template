#!/usr/bin/python
# -*- coding: utf-8 -*-

# Button properties:
#   button_texture      This is the background texture of the button
#   button_position
#   button_action       A string for what the button should do. To be interpreted by your program.
#   button_type         A string for the type of button (I use for confirm/cancel sounds). To be interpreted by your program.

# Buttons can have images and labels.

# Label properties:
#   label_text
#   label_position          Where to draw text (relative to button_position)
#   label_style             shadow, outline
#   label_color
#   label_secondary_color   Shadow/outline color
#   label_font              

# Image properties:
#   image_texture       This is an icon image that goes above the background texture
#   image_position      Where to draw image (relative to button_position)
#   image_blitrect      Rectangle position on the image to draw
#   image_effect        UNIMPLEMENTED

from Label import Label
from Image import Image

from copy import copy

class Button:
    def __init__( self, options=None ):
        self.options = {}
        self.backgroundImage = None
        self.backgroundPosition = None
        self.blitRect = None
        
        self.iconImage = None
        self.iconPosition = None
        self.iconBlitRect = None
        
        self.label = None
        self.image = None
        
        self.action = None
        self.tempTextPosition = None

        if ( options is not None ):
            self.Setup( options )
        
    def Setup( self, options ):
        # Store all options
        self.options = copy( options )
        print( options )
        
        if ( "button_texture" in self.options ):
            self.backgroundImage = self.options["button_texture"]
            
        if ( "button_position" in self.options ):
            self.backgroundPosition = self.options["button_position"]
            
        if ( "button_action" in self.options ):
            self.action = self.options["button_action"]

        if ( "button_type" in self.options ):
            self.type = self.options["button_type"]
    
        if ( "button_blitrect" in self.options ):
            self.blitRect = self.options["button_blitrect"]

        if ( "label_text" in self.options ):
            # Make position of label relative to button
            self.options["label_position"] = (self.backgroundPosition[0] + self.options["label_position"][0], self.backgroundPosition[1] + self.options["label_position"][1])
            self.label = Label()
            self.label.Setup( self.options )
            
        if ( "image_texture" in self.options ):
            # Make position of label relative to button
            self.options["image_position"] = (self.backgroundPosition[0] + self.options["image_position"][0], self.backgroundPosition[1] + self.options["image_position"][1])
            self.image = Image()
            self.image.Setup( self.options )    
    
    def GetAction( self ):
        return self.action
    
    def IsClicked( self, mouseX, mouseY ):
        rect = self.backgroundImage.get_rect()
        rect.x = rect.x + self.backgroundPosition[0]
        rect.y = rect.y + self.backgroundPosition[1]
        return ( mouseX >= rect.x and mouseX <= rect.x + rect.width and
                 mouseY >= rect.y and mouseY <= rect.y + rect.height )

    def Draw( self, windowSurface ):
        #print( self.blitRect )
        # Draw the background image
        if ( self.backgroundImage is not None ):
            if ( self.blitRect is not None ):
                windowSurface.blit( self.backgroundImage, self.backgroundPosition, self.blitRect )
            else:
                windowSurface.blit( self.backgroundImage, self.backgroundPosition )
        
        # Draw the icon
        if ( self.image is not None ):
            self.image.Draw( windowSurface )
        
        # Draw the text
        if ( self.label is not None ):
            self.label.Draw( windowSurface )

    def ChangeText( self, fgText, bgText=None ):
        if ( self.label is not None ):
            self.label.ChangeText( fgText, bgText )

    def SetStyle( self, newStyle ):
        self.style = newStyle
        
        if ( self.label is not None ):
            self.label.style = newStyle
            self.label.SetStyle()
        
    def SetPosition( self, x, y ):
        deltaX = x - self.fgTextPosition[0]
        deltaY = y - self.fgTextPosition[1]

        self.fgTextPosition[0] = x
        self.fgTextPosition[1] = y

        # Text background
        #if ( self.bgText is not None ):
            #for position in self.bgTextPositions:
                #windowSurface.blit( self.bgTextSurface, position
                
        for i in range( len( self.bgTextPositions ) ):
            self.bgTextPositions[i] = ( self.bgTextPositions[i][0] + deltaX, self.bgTextPositions[i][1] + deltaY )







