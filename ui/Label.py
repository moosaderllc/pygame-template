#!/usr/bin/python
# -*- coding: utf-8 -*-

# Label properties:
#   label_text
#   label_position          Where to draw text
#   label_style             shadow, outline
#   label_color             Text color
#   label_secondary_color   Shadow/outline color
#   label_font

from copy import copy

class Label:
    def __init__( self, options=None ):
        self.options = {}
        
        self.font = None

        self.fgText = None
        self.fgTextPosition = None
        self.fgTextColor = None

        self.bgText = None
        self.bgTextPosition = None
        self.bgTextColor = None
        self.bgTextPositions = []

        self.fgTextSurface = None
        self.bgTextSurface = None

        self.style = None

        self.tempTextPosition = None

        if ( options is not None ):
            self.Setup( options )

    def Setup( self, options ):
        # Store all options
        self.options = copy( options )
            
        if ( "label_font" in self.options ):
            self.font = self.options["label_font"]

        if ( "label_text" in self.options ):
            self.fgText = self.options["label_text"]

        if ( "label_position" in self.options ):
            self.tempTextPosition = options["label_position"]

        if ( "label_color" in self.options ):
            self.fgTextColor = options["label_color"]

        if ( "label_secondary_color" in self.options ):
            self.bgTextColor = self.options["label_secondary_color"]

        if ( "label_style" in self.options ):
            self.style = self.options["label_style"]
            self.bgText = self.fgText

        # Create text surface
        self.ChangeText( self.fgText, self.bgText )

    def ChangeText( self, fgText, bgText=None ):
        del self.bgTextPositions[:]
        self.fgText = fgText

        if ( bgText == None ):
            self.bgText = fgText
        else:
            self.bgText = bgText

        if ( self.fgText is not None ):
            self.fgTextSurface = self.font.render( self.fgText, True, self.fgTextColor )
            self.fgTextPosition = self.fgTextSurface.get_rect()
            self.fgTextPosition.topleft = ( self.tempTextPosition[0], self.tempTextPosition[1] )
            

        if ( self.style is not None ):
            self.bgTextSurface = self.font.render( self.bgText, True, self.bgTextColor )
            self.bgTextPosition = self.bgTextSurface.get_rect()

        self.SetStyle()
        if ( self.fgTextPosition is not None ):
            self.SetPosition( self.fgTextPosition[0], self.fgTextPosition[1] )

    def SetStyle( self ):
        if ( self.style == "shadow" ):
            self.bgTextPositions.append( ( self.tempTextPosition[0] + 2, self.tempTextPosition[1] + 2 ) )

        elif ( self.style == "outline" ):
            self.bgTextPositions.append( ( self.tempTextPosition[0] + 2, self.tempTextPosition[1] + 2 ) )
            self.bgTextPositions.append( ( self.tempTextPosition[0] + 0, self.tempTextPosition[1] + 2 ) )
            self.bgTextPositions.append( ( self.tempTextPosition[0] - 2, self.tempTextPosition[1] + 2 ) )

            self.bgTextPositions.append( ( self.tempTextPosition[0] + 2, self.tempTextPosition[1] - 2 ) )
            self.bgTextPositions.append( ( self.tempTextPosition[0] + 0, self.tempTextPosition[1] - 2 ) )
            self.bgTextPositions.append( ( self.tempTextPosition[0] - 2, self.tempTextPosition[1] - 2 ) )

            self.bgTextPositions.append( ( self.tempTextPosition[0] - 2, self.tempTextPosition[1] + 0 ) )
            self.bgTextPositions.append( ( self.tempTextPosition[0] + 2, self.tempTextPosition[1] + 0 ) )


    def SetPosition( self, x, y ):
        deltaX = x - self.fgTextPosition[0]
        deltaY = y - self.fgTextPosition[1]

        self.fgTextPosition[0] = x
        self.fgTextPosition[1] = y

        for i in range( len( self.bgTextPositions ) ):
            self.bgTextPositions[i] = ( self.bgTextPositions[i][0] + deltaX, self.bgTextPositions[i][1] + deltaY )
            
    def GetPosition( self ):
        return self.fgTextPosition

    def Draw( self, windowSurface ):
        # Text background
        if ( self.bgText is not None ):
            for position in self.bgTextPositions:
                windowSurface.blit( self.bgTextSurface, position )

        # Text foreground
        if ( self.fgText is not None ):
            windowSurface.blit( self.fgTextSurface, self.fgTextPosition )
